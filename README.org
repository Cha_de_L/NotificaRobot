* NotificaRobot
Un robot Telegram qui envoie des notifications.

NotificaRobot utilise la bibliothèque [[https://crates.io/crates/telegram-bot][telegram-bot]].

* Installation
Installer Rust à l’aide de [[https://rustup.rs/][rustup]].

NotificaRobot utilise la version de test (/nightly/) de Rust. Il faut donc
utiliser la commande suivante à la racine du projet :
#+BEGIN_SRC sh
rustup override set nightly
#+END_SRC

Il faut créer le fichier =env.toml=, voir le fichier d’exemple
=env.toml.example=.

Pour lancer le robot :
#+BEGIN_SRC sh
cargo run
#+END_SRC

Pour compiler le projet, afin de le déployer :
#+BEGIN_SRC sh
cargo build --release
#+END_SRC

L’exécutable obtenu peut ensuite être déployé sur un serveur, accompagné du
fichier =env.toml=. Pensez à utiliser la commande =strip= sur l’exécutable afin
de réduire sa taille.

* Licence
NotificaRobot utilise la licence publique générale GNU (GNU GPLv3). Celle-ci est
disponible dans le fichier =COPYING=.

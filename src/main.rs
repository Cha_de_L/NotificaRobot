// NotificaRobot est un robot Telegram qui envoie des notifications.
// Copyright (C) 2018  Charles de Lacombe <charles@de-lacom.be>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use futures::{future::lazy, Stream};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs};
use telegram_bot_fork::*;

#[derive(Deserialize, Debug)]
struct Config {
    token: String,
    file: String,
}

type GroupsMap = HashMap<Group, UsersMap>;

type UsersMap = HashMap<i64, User>;

type Database = rustbreak::FileDatabase<GroupsMap, rustbreak::deser::Ron>;

fn get_good_group(chat: &types::MessageChat) -> Option<Group> {
    match chat {
        types::MessageChat::Group(chat) => Some(Group::Group(chat.id.to_string())),
        types::MessageChat::Supergroup(chat) => Some(Group::SuperGroup(chat.id.to_string())),
        _ => None,
    }
}

fn get_list(db: &Database, group: &Group) -> UsersMap {
    let mut list: HashMap<i64, User> = HashMap::new();

    db.read(|db| {
        if let Some(s) = db.get(group) {
            list = s.to_owned();
        }
    })
    .unwrap();

    list
}

fn save_group(db: &Database, group: Group, list: UsersMap) -> rustbreak::error::Result<()> {
    db.write(|db| {
        db.insert(group, list);
    })
    .map(|()| db.save().unwrap())
}

#[derive(Serialize, Deserialize, Clone, Debug, Hash, PartialEq, Eq)]
enum Group {
    Group(String),
    SuperGroup(String),
}

#[derive(Serialize, Deserialize, Clone, Debug, Hash, PartialEq, Eq)]
struct User {
    id: i64,
    name: String,
}

impl User {
    fn link(&self) -> String {
        format!("[{}](tg://user?id={})", self.name, self.id)
    }
}

fn get_config() -> Config {
    let s = fs::read_to_string("env.toml").expect("file not found");
    toml::from_str(&s).unwrap()
}

#[derive(Debug)]
enum Command {
    Ping,
    Unping,
    Pingall,
}

fn get_command(message: &str, bot_name: &str) -> Option<Command> {
    use self::Command::*;

    if !message.starts_with("/") {
        return None;
    }

    let mut cmd = message.clone();

    if cmd.ends_with(bot_name) {
        cmd = cmd.rsplitn(2, '@').skip(1).next().unwrap();
    }

    match cmd {
        "/ping" => Some(Ping),
        "/pingall" => Some(Pingall),
        "/unping" => Some(Unping),
        _ => None,
    }
}

fn warn_bad_chat_type(api: &Api, message: Message) {
    api.spawn(
        message
            .chat
            .text("Ce robot peut uniquement être utilisé dans des groupes ou des supergroupes."),
    );
}

fn handle_message(api: &Api, db: &Database, message: Message) {
    match message.kind {
        MessageKind::Text { ref data, .. } => {
            println!("<{}>: {}", &message.from.first_name, data);

            let command = get_command(data, "NotificaRobot");

            println!("Command: {:?}", command);

            command.map(|cmd| match cmd {
                Command::Ping => {
                    if get_good_group(&message.chat).is_some() {
                        let keyboard = reply_markup!
                            (inline_keyboard,
                             ["Ici" callback "ping"]);

                        api.spawn(
                            message
                                .chat
                                .text("Pour vous ajouter à /pingall, cliquez sur ce bouton.")
                                .reply_markup(keyboard),
                        );
                    } else {
                        warn_bad_chat_type(api, message);
                    }
                }
                Command::Pingall => {
                    if let Some(group) = get_good_group(&message.chat) {
                        let list: String = get_list(&db, &group)
                            .iter()
                            .map(|(_, ref s)| s.link())
                            .collect::<Vec<String>>()
                            .join(", ");
                        api.spawn(message.chat.text(list).parse_mode(ParseMode::Markdown));
                    } else {
                        warn_bad_chat_type(api, message);
                    };
                }
                Command::Unping => {
                    if get_good_group(&message.chat).is_some() {
                        let keyboard = reply_markup!
                                (inline_keyboard,
                                 ["Là" callback "unping"]);

                        api.spawn(
                            message
                                .chat
                                .text("Pour vous retirer de /pingall, cliquez sur ce bouton.")
                                .reply_markup(keyboard),
                        );
                    } else {
                        warn_bad_chat_type(api, message);
                    }
                }
            });
        }
        MessageKind::LeftChatMember { data } => {
            get_good_group(&message.chat).map(|group| {
                let mut list = get_list(&db, &group);

                list.remove(&data.id.to_string().parse().unwrap())
                    .map(|user| {
                        println!(
                            "Removed {} from ping list because they left the group.",
                            user.name
                        );
                    });
            });
        }
        _ => {}
    };
}

fn handle_callback_query(api: &Api, db: &Database, query: CallbackQuery) {
    println!("Click from {}", query.from.first_name);

    let user_id = query.from.id.to_string().parse().unwrap();
    let user_name = query.from.first_name.to_owned();

    match query.data.as_str() {
        "ping" => {
            get_good_group(&query.message.chat).map(|group| {
                let mut list = get_list(&db, &group);

                list.insert(
                    user_id,
                    User {
                        id: user_id,
                        name: user_name,
                    },
                );
                match save_group(&db, group, list) {
                    Ok(()) => {
                        api.spawn(
                            query
                                .answer("Vous avez été ajouté à la liste de ce groupe.")
                                .show_alert(),
                        );
                    }
                    Err(e) => {
                        eprintln!("{}\n{:?}", e, query);
                    }
                };
            });
        }
        "unping" => {
            get_good_group(&query.message.chat).map(|group| {
                let mut list = get_list(&db, &group);

                list.remove(&user_id);
                match save_group(&db, group, list) {
                    Ok(()) => {
                        api.spawn(
                            query
                                .answer("Vous avez été retiré de la liste de ce groupe.")
                                .show_alert(),
                        );
                    }
                    Err(e) => {
                        eprintln!("{}\n{:?}", e, query);
                    }
                }
            });
        }
        _ => {}
    }
}

fn main() {
    let config = get_config();
    println!("{:#?}", config);

    let db = Database::from_path(&config.file, HashMap::new()).expect("Failed opening DB file.");
    db.load()
        .unwrap_or_else(|_| println!("Database not initialized, creating new one."));

    tokio::runtime::current_thread::Runtime::new()
        .unwrap()
        .block_on(lazy(|| {
            let api = Api::new(&config.token).unwrap();

            let stream = api.stream().then(|mb_update| {
                let res: Result<Result<Update, Error>, ()> = Ok(mb_update);
                res
            });

            stream.for_each(move |update| {
                match update {
                    Ok(update) => match update.kind {
                        UpdateKind::Message(message) => handle_message(&api, &db, message),
                        UpdateKind::CallbackQuery(query) => handle_callback_query(&api, &db, query),
                        _ => {}
                    },
                    Err(_) => {}
                }

                Ok(())
            })
        }))
        .unwrap();
}
